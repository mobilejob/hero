class CreateJobPostings < ActiveRecord::Migration[5.2]
  def change
    create_table :job_postings do |t|
      t.string :title
      t.string :keyword
      t.string :location
      t.text :description_company
      t.text :description_offer
      t.text :description_tasks
      t.text :description_team

      t.timestamps
    end
  end
end
