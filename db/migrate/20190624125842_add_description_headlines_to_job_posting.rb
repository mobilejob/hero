class AddDescriptionHeadlinesToJobPosting < ActiveRecord::Migration[5.2]
  def change
    add_column :job_postings, :headline_company, :string
    add_column :job_postings, :headline_offer, :string
    add_column :job_postings, :headline_tasks, :string
    add_column :job_postings, :headline_team, :string
  end
end
