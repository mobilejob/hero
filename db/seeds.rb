# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create(email: 'test@example.com', password: 'test12345')

JobPosting.create([
  {
    title: 'Senior Ruby on Rails Developer (m/w/d)',
    keyword: 'abcde',
    location: 'Karl-Liebknecht-Str. 5, 10178 Berlin',
    headline_company: 'Über uns',
    description_company: '<div>mobileJob.com ist aus der etventure-Familie entstanden und hat sich mithilfe namhafter Investoren zu einem vielversprechenden Start-up im HR-Markt entwickelt. Weit über 3000 Kunden sowie zahlreiche Start-up-Auszeichnungen zeigen: mobileJob.com gehört die Zukunft. Als junges Unternehmen arbeiten wir jeden Tag hart an unseren Zielen, wissen allerdings auch, wie man Erfolge gebührend feiert. Unser Unternehmensstandort Berlin bietet den idealen Rahmen dafür. Seit der Gründung im Jahr 2014 bieten wir unseren Mitarbeitern glänzende Perspektiven in einem wachsenden Markt.</div>',
    headline_offer: 'Das bringst Du mit',
    description_offer: '<ul><li>Ausbildung oder Studium mit IT technischem Background</li><li>10 Jahre Programmiererfahrung</li><li>Idealerweise mit verschiedenen Programmiersprachen&nbsp;</li><li>5 Jahre Web-Development Erfahrung</li><li>2-3 Jahre Erfahrung als RoR Developer, idealerweise als Full-Stack Developer</li><li>Deutsch oder Englisch auf C2 Niveau</li><li>Erfahrung in der Anwendung agiler Entwicklungsmethoden (z.B. Scrum)&nbsp;</li></ul>',
    headline_tasks: 'Was Dich bei uns erwartet',
    description_tasks: '<ul><li>Weiterentwicklung der mobileJob-Plattform mit Ruby on Rails&nbsp;</li><li>Anwendung agiler Entwicklungmethoden (Scrum-Methode)</li><li>Durchführen von Code-Reviews und Schreiben von Tests</li><li>Unterstützung bei der Konzeption und Implementierung von unserem noch unveröffentlichten System</li></ul>',
    headline_team: 'Das Team',
    description_team: '<div>Bei uns wirst du mit folgenden Techniken arbeiten:&nbsp;</div><ul><li>Backend:&nbsp; Ruby, Ruby on Rails, MySQL&nbsp;</li><li>Frontend:&nbsp; HAML, SASS, ES6</li><li>Infrastructure: GCP, optional DockerWenn du eine Vorliebe fürs Backend oder Frontend hast, bei uns kannst du wählen.<br><br>Das Team steht weiteren Techniken offen gegenüber und freut sich über neue Kollegen/innen, die Wissen über technische Trends und Entwicklungen mitbringen.<br><br>Wir sind ein wachsendes Team von aktuell 8 Entwicklern, aufgeteilt auf zwei Teams, 4 Product Ownern, 2 UX/UI Designern und&nbsp; einem Tester.</li></ul>'
  },{
    title: 'Advertising Manager (m/w/d)',
    keyword: 'fghij',
    location: 'Karl-Liebknecht-Str. 5, 10178 Berlin',
    headline_company: 'Über uns',
    description_company: '<div>mobileJob.com ist aus der etventure-Familie entstanden und hat sich mithilfe namhafter Investoren zu einem vielversprechenden Start-up im HR-Markt entwickelt. Weit über 3000 Kunden sowie zahlreiche Start-up-Auszeichnungen zeigen: mobileJob.com gehört die Zukunft. Als junges Unternehmen arbeiten wir jeden Tag hart an unseren Zielen, wissen allerdings auch, wie man Erfolge gebührend feiert. Unser Unternehmensstandort Berlin bietet den idealen Rahmen dafür. Seit der Gründung im Jahr 2014 bieten wir unseren Mitarbeitern glänzende Perspektiven in einem wachsenden Markt.</div>',
    headline_offer: 'Das bringst Du mit',
    description_offer: '<ul><li>Ein abgeschlossenes Studium im Bereich Wirtschaftswissenschaften, Kommunikation, Marketing oder eine vergleichbare Ausbildung</li><li>mind. 3 Jahre Berufserfahrung (Social-)Advertising inkl. Budgetverantwortung, insbesondere mit Facebook sowie Facebook-Marketing-Partnern, SEA, Webanalytics, Ad-Automation</li><li>Starker Fokus auf Conversion-Optimierung sowie datengetriebene, analytische Denkweise</li><li>Sicherer Umgang mit Google Tagmanager sowie Google Analytics</li><li>Ein hohes Interesse und Verständnis von AdTech und Tracking-Technologien</li></ul>',
    headline_tasks: 'Was Dich bei uns erwartet',
    description_tasks: '<ul><li>Als Teil unseres Performance-Marketing-Teams bist du verantwortlich für die Optimierungen und den Ausbau unserer Reichweitenkanäle</li><li>Du verantwortest die Objectives unserer Kampagnen, die Ausgestaltung unseres Targetings sowie der Creatives im Rahmen unserer KPI-Ziele</li><li>Identifikation, Bewertung und A/B-Testing von Potentialen zur strategischen Erweiterung unseres Reichweiten-Portfolios</li><li>Eigenständige Planung und Steuerung von Marketingkampagnen zur Leadgenerierung in den Bereichen B2C und B2B</li><li>Erstellung von Reports und Analysen sowie das Ableiten und Umsetzen von Optimierungen unserer Marketing-Kampagnen mit einem klaren ROI-Fokus</li></ul>',
    headline_team: 'Das bieten wir Dir',
    description_team: '<ul><li><strong>Perspektive: </strong>Wir sind ein neugieriges und ambitioniertes Team von Experten in einem stark wachsenden Markt und geben dir die Möglichkeit unser Produkt maßgeblich mitzugestalten. Wir bieten dir den Gestaltungsspielraum, den du für deine Ideen und anspruchsvollen Aufgaben benötigst, und die Unterstützung, um das beste aus dir und deinem Team herauszuholen und um unser Produkt kontinuierlich weiter zu entwickeln&nbsp;</li><li>&nbsp;<strong>Ein starkes Team: </strong>Unser Marketing-Team verfügt über weitreichende Erfahrungen in den Bereichen Advertising, CRM, Event Management, SEM, Social Media, Grafikdesign, Content und PR. Eine freundschaftliche Arbeitsatmosphäre ist unser Antriebsfaktor und Marketing unsere Leidenschaft. Wir unterstützen uns zu jederzeit, um das Beste aus uns und unserem Team rauszuholen.</li></ul>'
  },{
    title: 'Inside Sales Manager (m/w/d)',
    keyword: 'klmno',
    location: 'Karl-Liebknecht-Str. 5, 10178 Berlin',
    headline_company: 'Über uns',
    description_company: '<div>mobileJob.com ist aus der etventure-Familie entstanden und hat sich mithilfe namhafter Investoren zu einem vielversprechenden Start-up im HR-Markt entwickelt. Weit über 3000 Kunden sowie zahlreiche Start-up-Auszeichnungen zeigen: mobileJob.com gehört die Zukunft. Als junges Unternehmen arbeiten wir jeden Tag hart an unseren Zielen, wissen allerdings auch, wie man Erfolge gebührend feiert. Unser Unternehmensstandort Berlin bietet den idealen Rahmen dafür. Seit der Gründung im Jahr 2014 bieten wir unseren Mitarbeitern glänzende Perspektiven in einem wachsenden Markt.</div>',
    headline_offer: 'Das bringst Du mit',
    description_offer: '<ul><li>Eine Ausbildung oder Studium, idealerweise mit kaufmännischen Inhalten</li><li>Mind. 1 Jahr Erfahrung im B2B Vertrieb&nbsp;</li><li>Idealerweise Kenntnisse im Recruiting Umfeld</li><li>Idealerweise Kenntnisse des HR Marktes</li><li>Erfahrung im Sales-Pipeline-Management&nbsp;</li><li>Gute Kenntnisse eines CRM Tools (idealerweise Salesforce)</li><li>Ausgeprägte Deutschkenntnisse in Wort und Schrift</li><li>Englisch ist ein Plus</li></ul>',
    headline_tasks: 'Was Dich bei uns erwartet',
    description_tasks: '<ul><li>Telefonische Neukundenakquise von B2B Kunden in bestimmten Zielmärkten (z.B. Gesundheit &amp; Soziales, Logistik &amp; Verkehr, Facility Management, Call Center, Handel &amp; Konsum, Industrie)&nbsp;</li><li>Verantwortung von der Angebotserstellung bis hin zum Vertragsabschluss</li><li>Durchführung von Produktpräsentationen web-basiert und über Telefon &nbsp;</li><li>Dateneingabe und -pflege in unserem CRM Tool (Salesforce)&nbsp;</li><li>Regelmäßige Teilnahme an Events und Personalmessen&nbsp;</li></ul>',
    headline_team: 'Das bieten wir Dir',
    description_team: '<div>Am Standort Berlin suchen wir neue Kollegen für unser Sales Team, in dem Du dich schnell in deinem Tempo weiterentwickeln kannst bis zum Senior Sales Manager oder - wenn du magst - auch ins Account Management. Wir bezahlen ein attraktives Grundgehalt plus variablem Anteil und bieten die Wahl zwischen Voll- und Teilzeit (mind. 32 Std.) sowie ein schönes Büro in der 6. Etage direkt am Alexanderplatz.</div>'
  }]
)

