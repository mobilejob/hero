json.extract! job_posting, :id, :title, :keyword, :location, :headline_company, :description_company, :headline_offer, :description_offer, :headline_tasks, :description_tasks, :headline_team, :description_team, :created_at, :updated_at
json.url job_posting_url(job_posting, format: :json)
