require "application_system_test_case"

class JobPostingsTest < ApplicationSystemTestCase
  setup do
    @job_posting = job_postings(:one)
  end

  test "visiting the index" do
    visit job_postings_url
    assert_selector "h1", text: "Job Postings"
  end

  test "creating a Job posting" do
    visit job_postings_url
    click_on "New Job Posting"

    fill_in "Headline company", with: @job_posting.headline_company
    fill_in "Description company", with: @job_posting.description_company
    fill_in "Headline tasks", with: @job_posting.headline_tasks
    fill_in "Description tasks", with: @job_posting.description_tasks
    fill_in "Headline team", with: @job_posting.headline_team
    fill_in "Description team", with: @job_posting.description_team
    fill_in "Headline offer", with: @job_posting.headline_offer
    fill_in "Description offer", with: @job_posting.description_offer
    fill_in "Keyword", with: @job_posting.keyword
    fill_in "Location", with: @job_posting.location
    fill_in "Title", with: @job_posting.title
    click_on "Create Job posting"

    assert_text "Job posting was successfully created"
    click_on "Back"
  end

  test "updating a Job posting" do
    visit job_postings_url
    click_on "Edit", match: :first

    fill_in "Headline company", with: @job_posting.headline_company
    fill_in "Description company", with: @job_posting.description_company
    fill_in "Headline tasks", with: @job_posting.headline_tasks
    fill_in "Description tasks", with: @job_posting.description_tasks
    fill_in "Headline team", with: @job_posting.headline_team
    fill_in "Description team", with: @job_posting.description_team
    fill_in "Headline offer", with: @job_posting.headline_offer
    fill_in "Description offer", with: @job_posting.description_offer
    fill_in "Keyword", with: @job_posting.keyword
    fill_in "Location", with: @job_posting.location
    fill_in "Title", with: @job_posting.title
    click_on "Update Job posting"

    assert_text "Job posting was successfully updated"
    click_on "Back"
  end

  test "destroying a Job posting" do
    visit job_postings_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Job posting was successfully destroyed"
  end
end
