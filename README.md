# README

## Provisioning

1. docker-compose up -d web
1. docker-compose run web rails db:create
1. docker-compose run web rails db:migrate
1. docker-compose run web rails db:seed

## Console

1. docker-compose exec web bash

## Login

1. visit `http://localhost:3000`
1. username: test@example.com
1. password: test12345
